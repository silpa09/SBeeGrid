﻿$(document).ready(function () {

    var GridManagerObj = new GridManager();
    GridManagerObj.GridContainerObj = $('#GridContainer1');
    GridManagerObj.Columns = Colums;
    GridManagerObj.Data = Employees;
    GridManagerObj.ExtraData = ExtraData;
    GridManagerObj.SearchTextBoxObj = $('#SearchTb1');
    GridManagerObj.CellRender = function (RowElem, ColElem) {
        if (RowElem[ColElem.name + 'Data'] != null)
            return '<a href="' + RowElem[ColElem.name + 'Data'] + '">' + RowElem[ColElem.name] + '</a>';
        else
            return '<span>' + RowElem[ColElem.name] + '</span>';

    };
    GridManagerObj.Initiate()

    var GridManagerObj = new GridManager();
    GridManagerObj.GridContainerObj = $('#GridContainer2');
    GridManagerObj.Columns = Colums;
    GridManagerObj.Data = Employees;
    GridManagerObj.SearchTextBoxObj = $('#SearchTb2')
    GridManagerObj.Initiate()
});

var Colums = [
    { name: 'FirstName', type: 'text', caption: 'First Name' },
    { name: 'LastName', type: 'text', caption: 'Last Name' },
    { name: 'Age', type: 'number', caption: 'Age' },
    { name: 'Address', type: 'text', caption: 'Address' },
];

var Employees = [
    { FirstName: 'John', LastName: 'Shehata', Age: 34, Address: '507 - 20th Ave. E.Apt. 2A' },
    { FirstName: 'Andrew', LastName: 'Fuller', Age: 22, Address: '908 W. Capital Way' },
    { FirstName: 'Janet', LastName: 'Leverling', Age: 21, Address: '722 Moss Bay Blvd.' },
    { FirstName: 'Margaret', LastName: 'Peacock', Age: 36, Address: '4110 Old Redmond Rd.' },
    { FirstName: 'Steven', LastName: 'Buchanan', Age: 31, Address: '14 Garrett Hill' },
    { FirstName: 'Micho', LastName: 'Tiko', Age: null, Address: null },
];

var ExtraData = [
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: 'http://www.google.com' },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: 'http://www.google.com' },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: null },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: null },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: null },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: null },
];

